# Python module for PN-EN 80670-5-103
by pplaczek <p.dev>

---

Simple client(master) for communication with IEC-103 usage.

> [www.pplaczek.pl](https://www.pplaczek.pl/) *([blog](http://pplaczek.pl/blog/))*