"""
 * Copyrights (c) 2019 Piotr Płaczek
 * This file is protected by intellectual property law.
 * When using this file, please be aware of the terms of the MIT license.
"""
from pyIec103.service.client import Client
from pyIec103.view.text_view import TextView
import time

view = TextView()
client = Client()
client.set_view(view)
client.set_target(('10.20.15.12', 2404))
client.set_station_address(1)

if client.run():
    start = time.time()
    while (time.time() - start) < 180:
        client.poll()
        time.sleep(0.1)
    if client.stop():
        view.log('', 'Connection closed.', '')
    else:
        view.log('', 'Error during close socket', '')
else:
    view.log('', 'Error during open socket', '')
