
import abc


class View(abc.ABC):

    @classmethod
    @abc.abstractmethod
    def insert(cls, data):
        return True

    @classmethod
    @abc.abstractmethod
    def log(cls, pref, data, suff):
        return True