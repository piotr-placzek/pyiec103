import binascii
from pyIec103.view.view import View


class TextView(View):

    def __init__(self):
        return

    def __bytearray_to_hex_string__(self, data):
        return binascii.hexlify(bytearray(data))

    def log(self, pref, data, suff):
        if isinstance(data, bytearray):
            print(pref, self.__bytearray_to_hex_string__(data), suff)
        else:
            print(pref, data, suff)

    def insert(self, data):
        return

