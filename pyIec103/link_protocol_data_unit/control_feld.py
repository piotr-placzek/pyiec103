"""
 * Copyrights (c) 2019 Piotr Płaczek
 * This file is protected by intellectual property law.
 * When using this file, please be aware of the terms of the MIT license.
"""
from pyIec103.enums.control_field_bits import CtrlBits


class ControlField:

    def __init__(self, value):
        assert isinstance(value, int)
        self.__value = value

    def value(self):
        return self.__value

    def prm(self):
        return self.__value & CtrlBits.PRM.value >> 6

    def fcb(self):
        return self.__value & CtrlBits.FCB.value >> 5

    def fcv(self):
        return self.__value & CtrlBits.FCV.value >> 4

    def acd(self):
        return self.fcb()

    def dfc(self):
        return self.fcv()

    def function(self):
        return self.__value & 0x0F

    def set_function(self, fun):
        self.__value &= 0xF0
        self.__value += (fun & 0x0F)

    def set_value(self, value):
        self.__value = value

    def set(self, bit):
        if isinstance(bit, CtrlBits):
            self.__value |= bit.value
        elif CtrlBits.has_value(bit):
            self.__value |= bit
        else:
            assert False

    def reset(self, bit):
        if isinstance(bit, CtrlBits):
            self.__value &= ~bit.value
        elif CtrlBits.has_value(bit):
            self.__value &= ~bit
        else:
            assert False

    def switch(self, bit):
        if isinstance(bit, CtrlBits):
            self.__value ^= bit.value
        elif CtrlBits.has_value(bit):
            self.__value ^= bit
        else:
            assert False
