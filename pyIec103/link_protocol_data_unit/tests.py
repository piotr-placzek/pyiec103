"""
 * Copyrights (c) 2019 Piotr Płaczek
 * This file is protected by intellectual property law.
 * When using this file, please be aware of the terms of the MIT license.
"""
from pyIec103.enums.control_field_bits import CtrlBits
from pyIec103.link_protocol_data_unit.control_feld import ControlField
from pyIec103.link_protocol_data_unit.lpdu import Lpdu


def test_lpdu_valid():
    src = bytearray(b'\x10\x20\x01\x21\x16')
    lpdu = Lpdu()
    for i in src:
        tmp = bytearray(0)
        tmp.append(i)
        lpdu.collect(tmp)
    assert lpdu.get_lpdu() == src

    src = bytearray(b'\x68\x09\x09\x68\x08\x01\x08\x81\x0A\x01\xFF\x00\x1D\xB0\x16')
    lpdu = Lpdu()
    for i in src:
        tmp = bytearray(0)
        tmp.append(i)
        lpdu.collect(tmp)
    assert lpdu.get_lpdu() == src

    src = bytearray(b'\xE5')
    lpdu = Lpdu()
    for i in src:
        tmp = bytearray(0)
        tmp.append(i)
        lpdu.collect(tmp)
    assert lpdu.get_lpdu() == src


def test_lpdu_invalid():
    src = bytearray(b'\x10\x40\x01\x21\x16')
    lpdu = Lpdu()
    for i in src:
        tmp = bytearray(0)
        tmp.append(i)
        lpdu.collect(tmp)
    assert lpdu.get_lpdu() is not src

    src = bytearray(b'\x68\x09\x09\x68\x08\x01\x08\x81\x0A\x01\xFF\x00\x1D\xB0\x00')
    lpdu = Lpdu()
    for i in src:
        tmp = bytearray(0)
        tmp.append(i)
        lpdu.collect(tmp)
    assert lpdu.get_lpdu() is not src

    src = bytearray(b'\x00')
    lpdu = Lpdu()
    for i in src:
        tmp = bytearray(0)
        tmp.append(i)
        lpdu.collect(tmp)
    assert lpdu.get_lpdu() is not src


def test_ctrl():
    ctrl = ControlField(0x40)
    assert ctrl.value() == 0x40
    ctrl.set_function(0x0A)
    assert ctrl.value() == 0x4A
    ctrl.set_value(0x0A)
    assert ctrl.value() == 0x0A
    ctrl.set(CtrlBits.PRM)
    assert ctrl.value() == 0x4A
    ctrl.set(CtrlBits.FCB)
    assert ctrl.value() == 0x6A
    ctrl.set(CtrlBits.FCV)
    assert ctrl.value() == 0x7A
    ctrl.switch(CtrlBits.FCB)
    assert ctrl.value() == 0x5A
    ctrl.reset(CtrlBits.FCV)
    assert ctrl.value() == 0x4A
