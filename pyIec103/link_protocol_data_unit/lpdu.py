"""
 * Copyrights (c) 2019 Piotr Płaczek
 * This file is protected by intellectual property law.
 * When using this file, please be aware of the terms of the MIT license.
"""
from enum import Enum

from pyIec103.application_service_data_units.asdu import Asdu
from pyIec103.application_service_data_units.asdu1 import Asdu1
from pyIec103.enums import tid
from pyIec103.link_protocol_data_unit.control_feld import ControlField
from pyIec103.view.view import View


class Lpdu:

    class Stage(Enum):
        INIT1 = 0
        LEN1 = 1
        LEN2 = 2
        INIT2 = 3
        CTRL = 4
        CA = 5
        ASDU = 6
        CHECKSUM = 7
        STOP = 8
        ERROR = 9
        ACK = 0xE5

    def __init__(self):
        self.__stage = self.Stage.INIT1
        self.__lpdu = bytearray(b'')
        self.__ctrl = 0
        self.__asdu = bytearray(b'')
        self.__checksum = 0
        self.__asdu_size = 0
        self.__asdu_iterator = 0
        self.__is_ready = False
        self.__view = None

    def set_view(self, view):
        if isinstance(view, View):
            self.__view = view

    def collect(self, byte):
        byte = int(byte[0])
        self.__lpdu.append(byte)
        if self.__stage == self.Stage.INIT1:
            if byte == 0x10:
                self.__stage = self.Stage.CTRL
            elif byte == 0x68:
                self.__stage = self.Stage.LEN1
            elif byte == self.Stage.ACK:
                self.__stage = self.Stage.ACK
            else:
                return False
            return True
        if self.__stage == self.Stage.LEN1:
            self.__asdu_size = byte
            return True
        if self.__stage == self.Stage.LEN2:
            if byte == self.__asdu_size:
                self.__stage = self.Stage.CTRL
                return True
            else:
                self.__stage = self.Stage.ERROR
        if self.__stage == self.Stage.CTRL:
            self.__checksum += byte
            self.__ctrl = byte
            self.__stage = self.Stage.CA
            return True
        if self.__stage == self.Stage.CA:
            self.__checksum += byte
            self.__stage = self.Stage.ASDU
            return True
        if self.__stage == self.Stage.ASDU:
            if self.__asdu_iterator < self.__asdu_size:
                self.__asdu_iterator += 1
                self.__checksum += byte
                self.__asdu.append(byte)
                return True
            else:
                self.__stage = self.Stage.CHECKSUM
        if self.__stage == self.Stage.CHECKSUM:
            if byte == self.__checksum:
                self.__stage = self.Stage.STOP
                return True
            else:
                self.__stage = self.Stage.ERROR
        if self.__stage == self.Stage.STOP:
            if byte == 0x16:
                self.__is_ready = True
                if self.__view:
                    self.__view.log("Received:", self.__lpdu, "")
                return True
            else:
                self.__stage = self.Stage.ERROR
        if self.__stage == self.Stage.ERROR:
            return False
        if self.__stage == self.Stage.ACK:
            return True

    def is_ready(self):
        return self.__is_ready

    def get_ctrl(self):
        return ControlField(self.__ctrl)

    def get_asdu_object(self):
        if len(self.__asdu):
            return self.__cast_to_sec_obj__(Asdu(self.__asdu))
        else:
            return None

    def get_lpdu(self):
        return self.__lpdu

    def get_asdu(self):
        return self.__asdu

    @staticmethod
    def __cast_to_sec_obj__(asdu):
        assert isinstance(asdu, Asdu)
        if asdu.type() == tid.Tid.ASDU1:
            return Asdu1(asdu.data())
