"""
 * Copyrights (c) 2019 Piotr Płaczek
 * This file is protected by intellectual property law.
 * When using this file, please be aware of the terms of the MIT license.
"""
from enum import Enum


class CtrlFn(Enum):
    RESET_REMOTE_CONNECTION = 0
