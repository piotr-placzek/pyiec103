"""
 * Copyrights (c) 2019 Piotr Płaczek
 * This file is protected by intellectual property law.
 * When using this file, please be aware of the terms of the MIT license.
"""
from enum import Enum


class Cot(Enum):
    """
    Monitoring direction
    """
    SPONT = 1
    CYCLIC = 2
    RESET_FCB = 3
    RESET_CU = 4
    START = 5
    POWER_ON = 6
    TEST = 7
    GI = 9
    END_OF_GI = 10
    LOCAL = 11
    REMOTE = 12
    ACK_POS = 20
    ACK_NEG = 21
    WRITE_ACK_POS = 40
    WRITE_ACK_NEG = 41
    READ_DATA_POS = 42
    READ_DATA_NEG = 43
    WRITE_ACK = 44
    """
    Control direction
    """
    INIT_GI = 9
    GENERAL_ORDER = 20
    WRITE = 40
    READ = 42
    """
    Both directions
    """
    TIME_SYNC = 8
    DISTURBANCE_DATA_TRANSMISSION = 31
