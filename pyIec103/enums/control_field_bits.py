"""
 * Copyrights (c) 2019 Piotr Płaczek
 * This file is protected by intellectual property law.
 * When using this file, please be aware of the terms of the MIT license.
"""
from enum import Enum


class CtrlBits(Enum):
    PRM = 0x40
    FCB = 0x20
    FCV = 0x10
    ACD = 0x20
    DFC = 0x10

    @classmethod
    def has_value(cls, value):
        return any(value == item.value for item in cls)
