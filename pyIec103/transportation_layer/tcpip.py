"""
 * Copyrights (c) 2019 Piotr Płaczek
 * This file is protected by intellectual property law.
 * When using this file, please be aware of the terms of the MIT license.
"""
import time
import select
import socket

from pyIec103.link_protocol_data_unit.lpdu import Lpdu
from pyIec103.transportation_layer.transportation_layer import \
    TransportationLayer
from pyIec103.view.view import View


class TcpIpTl(TransportationLayer):

    def __init__(self, lpdu):
        assert isinstance(lpdu, Lpdu)
        self.__tcp_socket = None
        self.__target = ('127.0.0.1', 2404)
        self.__lpdu = lpdu
        self.__write_buf = []
        self.__view = None

    def set_view(self, view):
        if isinstance(view, View):
            self.__view = view
            self.__lpdu.set_view(view)

    def open(self):
        try:
            self.__tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.__tcp_socket.connect(self.__target)
            self.__tcp_socket.setblocking(False)
            return True
        except socket.error as error:
            if self.__view:
                self.__view.log("Connecting error:", str(error), "")
            return False

    def close(self):
        try:
            self.__tcp_socket.shutdown(socket.SHUT_RDWR)
            self.__tcp_socket.close()
            return True
        except socket.error as error:
            if self.__view:
                self.__view.log("Disconnecting error:", str(error), "")
            return False

    def write(self, data):
        if not isinstance(data, bytearray):
            return False
        self.__write_buf.append(data)

    def read(self):
        return

    def poll(self):
        try:
            res = [False, False, False]
            rr, rw, re = select.select([self.__tcp_socket],
                                       [self.__tcp_socket],
                                       [self.__tcp_socket],
                                       0.05)
            if rr:
                res = [self.__ready_read(), res[1], res[2]]
            if rw:
                res = [res[0], self.__ready_write(), res[2]]
            if re:
                res = [res[0], res[1], self.__ready_error()]
            return res
        except InterruptedError:
            return False

    def set_target(self, target_info):
        assert isinstance(target_info, tuple)
        self.__target = target_info

    def clear(self):
        self.__lpdu = Lpdu()
        self.__lpdu.set_view(self.__view)

    def get_lpdu(self):
        return self.__lpdu

    def __ready_read(self):
        self.__lpdu.collect(self.__tcp_socket.recv(1))
        return True

    def __ready_write(self):
        if self.__write_buf:
            try:
                fr = self.__write_buf.pop(0)
                bd = self.__tcp_socket.send(bytearray(fr))
                if bd == len(fr):
                    if self.__view:
                        self.__view.log("Sent:", bytearray(fr), "")
                    return True
                else:
                    return -1
            except InterruptedError:
                return -1
        return False

    def __ready_error(self):
        return True
