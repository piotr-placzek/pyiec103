"""
 * Copyrights (c) 2019 Piotr Płaczek
 * This file is protected by intellectual property law.
 * When using this file, please be aware of the terms of the MIT license.
"""
from pyIec103.link_protocol_data_unit import lpdu
from pyIec103.link_protocol_data_unit.lpdu import Lpdu
from pyIec103.transportation_layer.transportation_layer import \
    TransportationLayer


class DummyTl(TransportationLayer):

    def __init__(self, lpdu):
        assert isinstance(lpdu, Lpdu)
        self.__lpdu = lpdu
        self.__write_buf = bytearray(b'')
        pass

    def set_view(self, view):
        return

    def open(self):
        return True

    def close(self):
        return True

    def write(self, data):
        self.__write_buf = data
        return True

    def read(self):
        return self.__write_buf

    def poll(self):
        return [True, False, False]

    def set_target(self, target_info):
        pass

    def clear(self):
        self.__lpdu = Lpdu()

    def get_lpdu(self):
        return self.__lpdu
