"""
 * Copyrights (c) 2019 Piotr Płaczek
 * This file is protected by intellectual property law.
 * When using this file, please be aware of the terms of the MIT license.
"""
import abc


class TransportationLayer(abc.ABC):

    @classmethod
    @abc.abstractmethod
    def set_view(cls, view):
        return

    @classmethod
    @abc.abstractmethod
    def open(cls):
        return

    @classmethod
    @abc.abstractmethod
    def close(cls):
        return

    @classmethod
    @abc.abstractmethod
    def write(cls, data):
        return

    @classmethod
    @abc.abstractmethod
    def read(cls):
        return

    @classmethod
    @abc.abstractmethod
    def poll(cls):
        return

    @classmethod
    @abc.abstractmethod
    def set_target(cls, target_info):
        return

    @classmethod
    @abc.abstractmethod
    def clear(cls):
        return

    @classmethod
    @abc.abstractmethod
    def get_lpdu(cls):
        return
