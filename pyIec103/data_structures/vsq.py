"""
 * Copyrights (c) 2019 Piotr Płaczek
 * This file is protected by intellectual property law.
 * When using this file, please be aware of the terms of the MIT license.
"""
import struct


class Vsq:

    def __init__(self, data):
        assert isinstance(data, int)
        self.__value = data

    def value(self):
        return self.__value

    def cnt(self):
        assert (self.__value & 0x7F) < 10
        return self.__value & 0x7F

    def sq(self):
        return (self.__value & 0x80) >> 7
