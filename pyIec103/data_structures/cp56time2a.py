"""
 * Copyrights (c) 2019 Piotr Płaczek
 * This file is protected by intellectual property law.
 * When using this file, please be aware of the terms of the MIT license.
"""
from cp32time2a import CP32Time2a


class CP56Time2a(CP32Time2a):

    def __init__(self, data):
        super().__init__(data)
        self.__data = data

    def day_of_week(self):
        return (self.__data[4] >> 5) & 0x07

    def day_of_month(self):
        return self.__data[4] & 0x1f

    def month(self):
        return self.__data[5] & 0x0f

    def year(self):
        return self.__data[6] & 0x7f

    def res3(self):
        return (self.__data[5] >> 4) & 0x0f

    def res4(self):
        return (self.__data[6] >> 7) & 0x01
