"""
 * Copyrights (c) 2019 Piotr Płaczek
 * This file is protected by intellectual property law.
 * When using this file, please be aware of the terms of the MIT license.
"""


class Dpi:
    INVALID0 = 0
    OFF = 1
    ON = 2
    INVALID3 = 3

    def __init__(self, value):
        assert isinstance(value, int)
        assert self.INVALID0 <= value <= self.INVALID3
        self.__value = value

    def value(self):
        return self.__value
