"""
 * Copyrights (c) 2019 Piotr Płaczek
 * This file is protected by intellectual property law.
 * When using this file, please be aware of the terms of the MIT license.
"""
from cp32time2a import CP32Time2a
from cp56time2a import CP56Time2a
from dpi import Dpi
from vsq import Vsq


def test_dpi():
    dpi = Dpi(0)
    assert dpi.value() == Dpi.INVALID0
    dpi = Dpi(1)
    assert dpi.value() == Dpi.OFF
    dpi = Dpi(2)
    assert dpi.value() == Dpi.ON
    dpi = Dpi(3)
    assert dpi.value() == Dpi.INVALID3


def test_cp32time2a():
    time = CP32Time2a(bytearray(b'\x5F\xEA\x3B\x97'))
    assert time.milliseconds() == 59999
    assert time.minutes() == 59
    assert time.iv() == 0
    assert time.hours() == 23
    assert time.su() == 1
    assert time.res1() == 0
    assert time.res2() == 0


def test_cp56time2a():
    time = CP56Time2a(bytearray(b'\x5F\xEA\x3B\x97\xFF\x0C\x63'))
    assert time.milliseconds() == 59999
    assert time.minutes() == 59
    assert time.iv() == 0
    assert time.hours() == 23
    assert time.su() == 1
    assert time.res1() == 0
    assert time.res2() == 0
    assert time.day_of_week() == 7
    assert time.day_of_month() == 31
    assert time.month() == 12
    assert time.year() == 99
    assert time.res3() == 0
    assert time.res4() == 0


def test_vsq():
    vsq = Vsq(0x89)
    assert vsq.value() == 137
    assert vsq.cnt() == 9
    assert vsq.sq() == 1
