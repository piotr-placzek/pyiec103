"""
 * Copyrights (c) 2019 Piotr Płaczek
 * This file is protected by intellectual property law.
 * When using this file, please be aware of the terms of the MIT license.
"""


class CP32Time2a:

    def __init__(self, data):
        assert isinstance(data, bytearray)
        assert len(data) >= 4
        self.__data = data

    def milliseconds(self):
        ms = self.__data[1]
        ms = ms << 8
        ms = ms + self.__data[0]
        return ms

    def minutes(self):
        return self.__data[2] & 0x3F

    def res1(self):
        return (self.__data[2] >> 6) & 0x01

    def iv(self):
        return (self.__data[2] >> 7) & 0x01

    def hours(self):
        return self.__data[3] & 0x1f

    def res2(self):
        return (self.__data[3] >> 5) & 0x03

    def su(self):
        return (self.__data[3] >> 7) & 0x01
