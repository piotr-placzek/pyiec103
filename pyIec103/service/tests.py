"""
 * Copyrights (c) 2019 Piotr Płaczek
 * This file is protected by intellectual property law.
 * When using this file, please be aware of the terms of the MIT license.
"""
from pyIec103.link_protocol_data_unit.control_feld import ControlField
from pyIec103.link_protocol_data_unit.lpdu import Lpdu
from pyIec103.transportation_layer.dummy import DummyTl
from pyIec103.service.client import Client


def test_client():
    # Prapare
    lpdu = Lpdu()
    lpdu.collect(b'\x10')
    lpdu.collect(b'\x20')
    lpdu.collect(b'\x01')
    lpdu.collect(b'\x21')
    lpdu.collect(b'\x16')
    tl = DummyTl(lpdu)
    client = Client()
    client._Client__tl = tl
    client._Client__gen_frame = bytearray(b'\x10\x00\x00\x00\x16')

    # Test: setting station address
    client.set_station_address(0x01)
    assert client._Client__gen_frame[2] == 0x01, "Client.set_station_address"
    assert client._Client__gen_frame[3] == 0x01, "Client.set_station_address"

    # Test: update control field
    client.__update_ctrl__(0x20)
    assert client._Client__gen_frame[1] == 0x20, "Client.__update_ctrl__"
    assert client._Client__gen_frame[2] == 0x01, "Client.__update_ctrl__"
    assert client._Client__gen_frame[3] == 0x21, "Client.__update_ctrl__"

    # Test: __tl_poll (should return control)
    tmp = client.__tl_poll__()
    assert isinstance(tmp, ControlField), "Client.__tl_poll__"
    assert tmp.value() == 0x20

    # Test: client poll
    tmp = client.poll()
    assert tmp[0], 'Client.poll'
    assert tmp[1][0], 'Client.poll'
    client.poll()
    assert tl.read() == bytearray(b'\x10\x7A\x01\x7B\x16'), 'Client.poll'
    client.poll()
    assert tl.read() == bytearray(b'\x10\x5A\x01\x5B\x16'), 'Client.poll'
    client.poll()
    assert tl.read() == bytearray(b'\x10\x7A\x01\x7B\x16'), 'Client.poll'

    tl.clear()
    tl._DummyTl__lpdu.collect(b'\x10')
    tl._DummyTl__lpdu.collect(b'\x08')
    tl._DummyTl__lpdu.collect(b'\x01')
    tl._DummyTl__lpdu.collect(b'\x09')
    tl._DummyTl__lpdu.collect(b'\x16')

    client.poll()
    assert tl.read() == bytearray(b'\x10\x5B\x01\x5C\x16'), 'Client.poll'
    client.poll()
    assert tl.read() == bytearray(b'\x10\x7B\x01\x7C\x16'), 'Client.poll'
    client.poll()
    assert tl.read() == bytearray(b'\x10\x5B\x01\x5C\x16'), 'Client.poll'


def test_asdu_repo():
    pass