"""
 * Copyrights (c) 2019 Piotr Płaczek
 * This file is protected by intellectual property law.
 * When using this file, please be aware of the terms of the MIT license.
"""
from pyIec103.enums.control_field_bits import CtrlBits
from pyIec103.link_protocol_data_unit.control_feld import ControlField
from pyIec103.link_protocol_data_unit.lpdu import Lpdu
from pyIec103.service.asdu_repo import AsduRepo
from pyIec103.transportation_layer.tcpip import TcpIpTl
from pyIec103.view.view import View


class Client:

    def __init__(self):
        self.__tl = TcpIpTl(Lpdu())
        self.__repo = AsduRepo
        self.__is_running = False
        self.__ctrl = ControlField(0)
        self.__gen_frame = bytearray(b'\x10\x40\x01\x41\x16')
        self.__tl_poll_res = [False, False, False]
        self.__view = None

    def __update_ctrl__(self, data):
        if isinstance(data, ControlField):
            self.__ctrl = data
            self.__gen_frame[1] = data.value()
        elif isinstance(data, int):
            self.__ctrl.set_value(data)
            self.__gen_frame[1] = data
        self.__update_checksum__()

    def __update_checksum__(self):
        self.__gen_frame[3] = self.__gen_frame[1] + self.__gen_frame[2]

    def __prepare_reset_link(self):
        self.__ctrl.set_value(0x47)
        self.__update_ctrl__(self.__ctrl)

    def __tl_poll__(self):
        self.__tl_poll_res = self.__tl.poll()
        res = self.__tl_poll_res
        if res:
            if res[0]:
                if self.__tl.get_lpdu().is_ready():
                    lpdu_ctrl = self.__tl.get_lpdu().get_ctrl()
                    asdu = self.__tl.get_lpdu().get_asdu_object()
                    if asdu:
                        self.__repo.update(asdu)
                    # self.__tl.clear()
                    return lpdu_ctrl
        return False

    def set_target(self, target_info):
        self.__tl.set_target(target_info)

    def set_station_address(self, address):
        self.__gen_frame[2] = address & 0xFF
        self.__update_checksum__()

    def set_view(self, view):
        if isinstance(view, View):
            self.__view = view
            self.__tl.set_view(view)

    def get_data(self):
        return self.__repo

    def run(self):
        self.__is_running = self.__tl.open()
        if self.__is_running:
            self.__prepare_reset_link()
            self.__tl.write(self.__gen_frame)
        return self.__is_running

    def stop(self):
        self.__is_running = not self.__tl.close()
        return not self.__is_running

    def poll(self):
        ctrl = self.__tl_poll__()
        if ctrl:
            if ctrl.value() & 0x20:
                self.__ctrl.set_function(0x0A)
            else:
                self.__ctrl.set_function(0x0B)
            self.__ctrl.set(CtrlBits.PRM)
            self.__ctrl.set(CtrlBits.FCV)
            self.__ctrl.switch(CtrlBits.FCB)
            self.__update_ctrl__(self.__ctrl)
            self.__tl.write(self.__gen_frame)

            return True, self.__tl_poll_res
        return False, self.__tl_poll_res
