"""
 * Copyrights (c) 2019 Piotr Płaczek
 * This file is protected by intellectual property law.
 * When using this file, please be aware of the terms of the MIT license.
"""
from pyIec103.application_service_data_units.asdu import Asdu


class AsduRepo:

    def __init__(self):
        self.__repo = {}

    def update(self, asdu):
        assert isinstance(asdu, Asdu)
        self.__repo.update({(asdu.fun(), asdu.inf()), asdu})

    def read(self, ftin):
        assert isinstance(ftin, tuple)
        if ftin in self.__repo:
            return self.__repo[ftin]
        else:
            return False

    def clear(self):
        self.__repo = {}
