"""
 * Copyrights (c) 2019 Piotr Płaczek
 * This file is protected by intellectual property law.
 * When using this file, please be aware of the terms of the MIT license.
"""
from pyIec103.enums.cot import Cot


class Asdu:

    def __init__(self, data):
        assert isinstance(data, bytearray)
        self.__data = data

    def type(self):
        return self.__data[0]

    def vsq(self):
        return self.__data[1]

    def cot(self):
        return Cot(self.__data[2])

    def ca(self):
        return self.__data[3]

    def fun(self):
        return self.__data[4]

    def inf(self):
        return self.__data[5]

    def data(self):
        return self.__data