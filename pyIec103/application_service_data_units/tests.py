"""
 * Copyrights (c) 2019 Piotr Płaczek
 * This file is protected by intellectual property law.
 * When using this file, please be aware of the terms of the MIT license.
"""
from pyIec103.application_service_data_units.asdu1 import Asdu1
from pyIec103.data_structures.dpi import Dpi
from pyIec103.enums.cot import Cot


def test_asdu1():
    asdu = Asdu1(bytearray(b'\x01\x81\x01\x01\x10\x11\x02\x5F\xEA\x3B\x97\xFF'))
    assert asdu.type() == 0x01
    assert asdu.vsq() == 0x81
    assert asdu.cot() == Cot.SPONT
    assert asdu.ca() == 0x01
    assert asdu.fun() == 0x10
    assert asdu.inf() == 0x11
    assert asdu.dpi().value() == Dpi.ON
    assert asdu.sin() == 0xFF
    assert asdu.time().milliseconds() == 59999
    assert asdu.time().minutes() == 59
    assert asdu.time().iv() == 0
    assert asdu.time().hours() == 23
    assert asdu.time().su() == 1
    assert asdu.time().res1() == 0
    assert asdu.time().res2() == 0
