"""
 * Copyrights (c) 2019 Piotr Płaczek
 * This file is protected by intellectual property law.
 * When using this file, please be aware of the terms of the MIT license.
"""
from pyIec103.application_service_data_units.asdu import Asdu
from pyIec103.data_structures.cp32time2a import CP32Time2a
from pyIec103.data_structures.dpi import Dpi


class Asdu1(Asdu):

    def __init__(self, data):
        assert isinstance(data, bytearray)
        assert len(data) == 12
        super().__init__(data[0:6])
        self.__data = data

    def dpi(self):
        return Dpi(self.__data[6])

    def time(self):
        return CP32Time2a(self.__data[7:11])

    def sin(self):
        return self.__data[11]
